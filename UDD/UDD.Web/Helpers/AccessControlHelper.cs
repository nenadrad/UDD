﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using UDD.DataAccess.Model;

namespace UDD.Web.Helpers
{
    public class AccessControlHelper
    {
        public bool BookCanBeDownloaded(int bookCategoryId, ClaimsIdentity userIdentity)
        {
            var userRole = userIdentity.FindFirst(ClaimTypes.Role).Value;
            var subscribedCategory = userIdentity.FindFirst("bookCategory").Value;

            if (userRole == "Admin" || subscribedCategory == "All") return true;

            if (subscribedCategory == bookCategoryId.ToString()) return true;

            return false;
        }
    }
}