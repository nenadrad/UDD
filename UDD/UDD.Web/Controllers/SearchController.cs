﻿using Nest;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Mvc;
using UDD.DataAccess.Model;
using UDD.Web.Helpers;
using UDD.Web.Models;
using UDD.Web.Services;
using UDD.Web.Sevices;

namespace UDD.Web.Controllers
{
    [AllowAnonymous]
    public class SearchController : Controller
    {
        private BooksDBEntities db = new BooksDBEntities();
        private AccessControlHelper _accessControlHelper = new AccessControlHelper();

        // GET: Search
        public ActionResult Index()
        {
            var selectList = new List<SelectListItem>();
            var languages = db.Language;
            foreach(var language in languages)
            {
                var selectItem = new SelectListItem
                {
                    Text = language.Name,
                    Value = language.Name
                };
                selectList.Add(selectItem);
            }
            var defaultSelectItem = new SelectListItem
            {
                Text = "All",
                Value = null
            };
            selectList.Insert(0, defaultSelectItem);

            ViewBag.Language = selectList;
            return View();
        }

        public ActionResult Search(SearchModel searchModel)
        {
            var titleQuery = QueryGenerator.GenerateTitleQuery(searchModel.Title);
            var authorQuery = QueryGenerator.GenerateAuthorQuery(searchModel.Author);
            var contentQuery = QueryGenerator.GenerateContentQuery(searchModel.Content);
            var languageQuery = QueryGenerator.GenerateLanguageQuery(searchModel.Language);
            var keywordsQuery = QueryGenerator.GenerateKeywordsQuery(searchModel.Keywords);

            var query = new QueryContainer();

            var books = new List<SearchResult>();
            var responses = new List<IHit<PdfBook>>(); 

            if (searchModel.MatchAny)
                query = titleQuery || authorQuery || contentQuery || languageQuery || keywordsQuery;
            else
                query = titleQuery && authorQuery && contentQuery && languageQuery && keywordsQuery;
            

            var searchResponse = ElasticSearchService.Instance.Client.Search<PdfBook>(q => q.Query(query).Highlight(h => h.PreTags("<b><em>").PostTags("</b></em>").OnFields(f => f.OnField(e => e.Content).Type(HighlighterType.Plain).ForceSource().FragmentSize(150).NumberOfFragments(3).NoMatchSize(150))));

            var results = new List<SearchResult>();
            var userIdentity = User.Identity as ClaimsIdentity;
            foreach (var searchResponseHit in searchResponse.Hits)
            {
                var bookCategoryId = db.Book.Find(Convert.ToInt32(searchResponseHit.Id)).CategoryId;
                var searchResult = new SearchResult
                {
                    Title = searchResponseHit.Source.Title,
                    BookId = searchResponseHit.Source.Id,
                    Preview = string.Join("...<br/>", searchResponseHit.Highlights["content"].Highlights),
                    CanBeDownloaded = User.Identity.IsAuthenticated && _accessControlHelper.BookCanBeDownloaded(bookCategoryId, userIdentity)
                };
                results.Add(searchResult);
            }

            return View("SearchResults", results);
        }

        public FileResult Download(int bookId)
        {
            var bookFileName = db.Book.Find(bookId).FileName;
            var fileBytes = System.IO.File.ReadAllBytes(System.IO.Path.Combine(Server.MapPath("~/UploadedBooks"), bookFileName));
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, bookFileName);
        }
    }
}