﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UDD.DataAccess.Model;

namespace UDD.Web.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private BooksDBEntities db = new BooksDBEntities();

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var user = db.User.Include(u => u.Category).Include(u => u.Role);
            return View(user.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            var categorySelectList = new List<SelectListItem>();
            var categories = db.Category;
            foreach (var category in categories)
            {
                var selectItem = new SelectListItem
                {
                    Text = category.Name,
                    Value = category.Id.ToString()
                };
                categorySelectList.Add(selectItem);
            }
            var defaultSelectItem = new SelectListItem
            {
                Text = "",
                Value = null
            };
            categorySelectList.Insert(0, defaultSelectItem);

            ViewBag.CategoryId = categorySelectList;
            ViewBag.RoleId = new SelectList(db.Role, "Id", "Name");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Username,Password,RoleId,CategoryId")] User user)
        {
            if (ModelState.IsValid)
            {
                db.User.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", user.CategoryId);
            ViewBag.RoleId = new SelectList(db.Role, "Id", "Name", user.RoleId);
            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id, string returnUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var categorySelectList = new List<SelectListItem>();
            var categories = db.Category;
            foreach (var category in categories)
            {
                var selectItem = new SelectListItem
                {
                    Text = category.Name,
                    Value = category.Id.ToString(),
                    Selected = user.CategoryId == category.Id
                };
                categorySelectList.Add(selectItem);
            }
            var defaultSelectItem = new SelectListItem
            {
                Text = "",
                Value = null
            };
            defaultSelectItem.Selected = user.CategoryId == null;
            categorySelectList.Insert(0, defaultSelectItem);

            ViewBag.CategoryId = categorySelectList;
            ViewBag.RoleId = new SelectList(db.Role, "Id", "Name", user.RoleId);
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Username,Password,RoleId,CategoryId")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { id = user.Id });
            }
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", user.CategoryId);
            ViewBag.RoleId = new SelectList(db.Role, "Id", "Name", user.RoleId);
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.User.Find(id);
            db.User.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
