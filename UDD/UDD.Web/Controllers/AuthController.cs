﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using UDD.DataAccess.Model;
using UDD.Web.Models;

namespace UDD.Web.Controllers
{
    public class AuthController : Controller
    {
        private BooksDBEntities db = new BooksDBEntities();

        // GET: Auth
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = db.User.FirstOrDefault(u => u.Username == model.Username);
            if(user == null)
            {
                ModelState.AddModelError("", "User does not exist.");
                return View();
            }
            if(user.Password != model.Password)
            {
                ModelState.AddModelError("", "The password is not correct.");
                return View();
            }

            var identity = CreateUserIdentity(user);
            SignIn(identity);

            return RedirectToLocal(returnUrl);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            SignOut();
            return RedirectToAction("Index", "Books");
        }

        // GET: Auth
        [HttpGet]
        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            if (model.PasswordFirstInput != model.PasswordSecondInput)
            {
                ModelState.AddModelError("", "Passwords don't match.");
                return View();
            }

            var user = db.User.Find(Convert.ToInt32(User.Identity.GetUserId()));
            user.Password = model.PasswordFirstInput;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            SignOut();
            return RedirectToAction("Info", "Home", new { message = "Password successfully changed. Please log in again." });
        }

        // GET: Auth
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            var categorySelectList = new List<SelectListItem>();
            var categories = db.Category;
            foreach (var category in categories)
            {
                var selectItem = new SelectListItem
                {
                    Text = category.Name,
                    Value = category.Id.ToString()
                };
                categorySelectList.Add(selectItem);
            }
            var defaultSelectItem = new SelectListItem
            {
                Text = "",
                Value = null
            };
            categorySelectList.Insert(0, defaultSelectItem);

            ViewBag.CategoryId = categorySelectList;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User user)
        {
            user.RoleId = 2;

            if (ModelState.IsValid)
            {
                db.User.Add(user);
                db.SaveChanges();
                return RedirectToAction("Info", "Home", new { message = "Registration was successful. Please log in." });
            }

            return RedirectToAction("Info", "Home", new { message = "Registration was not successful. Please proceed to login." });
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Books");
        }

        private ClaimsIdentity CreateUserIdentity(User user)
        {
            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.Username));
            identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.Name));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));

            var subscribedCategory = user.CategoryId == null ? "All" : user.CategoryId.ToString();
            identity.AddClaim(new Claim("bookCategory", subscribedCategory));

            return identity;
        }

        private void SignIn(ClaimsIdentity identity)
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignIn(identity);
        }

        private void SignOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut();
        }
    }
}