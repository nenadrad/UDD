﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace UDD.Web.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ActionResult UploadFile(HttpPostedFileBase upload)
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    var files = Request.Files;
                    var file = files[0];
                    string fileName;

                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        var testFiles = file.FileName.Split('\\');
                        fileName = testFiles[testFiles.Length - 1];
                    }
                    else
                    {
                        fileName = file.FileName;
                    }
                    var path = Path.Combine(Server.MapPath("~/UploadedBooks"), fileName);
                    var contentType = file.ContentType;

                    file.SaveAs(path);

                    var document = PdfReader.Open(path);
                    var title = document.Info.Title;
                    var autor = document.Info.Author;
                    var keywords = document.Info.Keywords;

                    return Json(new {Title = title, FileName = fileName, MIME = contentType, Author = autor, Keywords = keywords }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            return Json("No files selected");
        }
    }
}