﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PdfSharp.Pdf.IO;
using TikaOnDotNet.TextExtraction;
using UDD.DataAccess.Model;
using UDD.Web.Models;
using UDD.Web.Sevices;
using Path = System.IO.Path;
using Nest;
using System.Security.Claims;
using UDD.Web.Helpers;
using System;
using Microsoft.AspNet.Identity;

namespace UDD.Web.Controllers
{
    public class BooksController : Controller
    {
        private BooksDBEntities db = new BooksDBEntities();
        private AccessControlHelper _accessControlHelper = new AccessControlHelper();

        // GET: Books
        [AllowAnonymous]
        public ActionResult Index()
        {
            var userIdentity = User.Identity as ClaimsIdentity;
            var books = db.Book.Include(c => c.Category).Include(u => u.User);
            if (User.Identity.IsAuthenticated)
            {
                foreach (var book in books)
                {
                    book.CanBeDownloaded = _accessControlHelper.BookCanBeDownloaded(book.CategoryId, userIdentity);
                }
            }
            return View(books.ToList());
        }

        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Book.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name");
            ViewBag.LanguageId = new SelectList(db.Language, "Id", "Name");
            ViewBag.UserId = new SelectList(db.User, "Id", "FirstName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CategoryId,LanguageId,UserId,Title,FileName,Author,Keywords,PublicationYear,MIME")] Book book)
        {
            if (ModelState.IsValid)
            {
                book.UserId = Convert.ToInt32(User.Identity.GetUserId());
                db.Book.Add(book);
                db.SaveChanges();
                book.Language = db.Language.First(l => l.Id == book.LanguageId);
                var bookPath = Path.Combine(Server.MapPath("~/UploadedBooks"), book.FileName);
                UpdateBookMetadata(book, bookPath);
                IndexBook(book, bookPath);
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", book.CategoryId);
            ViewBag.LanguageId = new SelectList(db.Language, "Id", "Name", book.LanguageId);
            ViewBag.UserId = new SelectList(db.User, "Id", "FirstName", book.UserId);
            return View(book);
        }

        private void UpdateBookMetadata(Book book, string bookPath)
        {
            var document = PdfReader.Open(bookPath);
            document.Info.Title = book.Title;
            document.Info.Author = book.Author;
            document.Info.Keywords = book.Keywords;
            document.Save(bookPath);
        }

        private void IndexBook(Book book, string bookPath)
        {
            var textExtractor = new TextExtractor();
            var result = textExtractor.Extract(bookPath);

            var pdfBook = new PdfBook
            {
                Content = result.Text,
                Title = book.Title,
                Author = book.Author,
                Language = book.Language.Name,
                Id = book.Id,
                Keywords = book.Keywords
            };

            ElasticSearchService.Instance.Client.Index(pdfBook);
        }

        // GET: Books/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Book.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", book.CategoryId);
            ViewBag.LanguageId = new SelectList(db.Language, "Id", "Name", book.LanguageId);
            ViewBag.UserId = new SelectList(db.User, "Id", "FirstName", book.UserId);
            return View(book);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CategoryId,LanguageId,UserId,Title,FileName,Author,Keywords,PublicationYear,MIME")] Book book)
        {
            if (ModelState.IsValid)
            {
                book.UserId = Convert.ToInt32(User.Identity.GetUserId());
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                book.Language = db.Language.First(l => l.Id == book.LanguageId);
                var bookPath = Path.Combine(Server.MapPath("~/UploadedBooks"), book.FileName);
                UpdateBookMetadata(book, bookPath);
                IndexBook(book, bookPath);
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Category, "Id", "Name", book.CategoryId);
            ViewBag.LanguageId = new SelectList(db.Language, "Id", "Name", book.LanguageId);
            ViewBag.UserId = new SelectList(db.User, "Id", "FirstName", book.UserId);
            return View(book);
        }

        // GET: Books/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Book.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Book.Find(id);
            db.Book.Remove(book);
            db.SaveChanges();
            ElasticSearchService.Instance.Client.Delete(new DeleteRequest<PdfBook>(id));
            return RedirectToAction("Index");
        }

        public FileResult Download(string filename)
        {
            var fileBytes = System.IO.File.ReadAllBytes(System.IO.Path.Combine(Server.MapPath("~/UploadedBooks"), filename));
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
