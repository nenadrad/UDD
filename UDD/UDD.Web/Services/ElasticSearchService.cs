﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Nest;

namespace UDD.Web.Sevices
{
    public sealed class ElasticSearchService
    {
        private static readonly ElasticSearchService instance = new ElasticSearchService();

        private ElasticSearchService()
        {
            var uri = ConfigurationManager.AppSettings["ElasticSearchHostUrl"];
            var node = new Uri(uri);
            var settings = new ConnectionSettings(node, "books");
            Client = new ElasticClient(settings);
        }

        public static ElasticSearchService Instance
        {
            get { return instance; }
        }

        public ElasticClient Client { get; set; }
    }
}