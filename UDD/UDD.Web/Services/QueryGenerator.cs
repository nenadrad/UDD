﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UDD.Web.Models;

namespace UDD.Web.Services
{
    public class QueryGenerator
    {
        public static QueryContainer GenerateAuthorQuery(string author)
        {
            return Query<PdfBook>.Match(m =>
                                        m.OnField(e =>
                                            e.Author).Query(author).Fuzziness(2));
        }

        public static QueryContainer GenerateContentQuery(string content)
        {
            return Query<PdfBook>.MatchPhrase(m =>
                                        m.OnField(e =>
                                            e.Content).Query(content));
        }

        public static QueryContainer GenerateFuzzyQuery(string fieldName, string value, string fuzziness)
        {
            return new FuzzyStringQuery
            {
                Field = fieldName,
                Value = value,
                Fuzziness = fuzziness
            };
        }

        public static QueryContainer GenerateKeywordsQuery(string keywords)
        {
            return string.IsNullOrWhiteSpace(keywords) ? Query<PdfBook>.Terms("keywords", null) : Query<PdfBook>.Terms("keywords", keywords.ToLower().Split(' '));
        }

        public static QueryContainer GenerateLanguageQuery(string language)
        {
            return language == "All" ? Query<PdfBook>.Term(t => t.Language, null) : Query<PdfBook>.Term(t => t.Language, language.ToLower());
        }

        public static QueryContainer GeneratePhraseQuery(string fieldName, string value)
        {
            return Query<PdfBook>
                .MatchPhrase(m =>
                    m.OnField(fieldName).Query(value));
        }

        public static QueryContainer GenerateTitleQuery(string title)
        {
            return Query<PdfBook>.Match(m =>
                                        m.OnField(e =>
                                            e.Title).Query(title).Fuzziness(2));
        }
    }
}