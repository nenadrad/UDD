﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nest;

namespace UDD.Web.Models
{
    public class PdfBook
    {
        [ElasticProperty(Name = "_id", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.Integer)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Language { get; set; }
        public string Keywords { get; set; }
        public string Content { get; set; }
    }
}