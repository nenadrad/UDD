﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UDD.Web.Models
{
    public class ResetPasswordModel
    {
        [DataType(DataType.Password)]
        public string PasswordFirstInput { get; set; }
        [DataType(DataType.Password)]
        public string PasswordSecondInput { get; set; }
    }
}