﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UDD.Web.Models
{
    public class SearchResult
    {
        public string Title { get; set; }
        public string Preview { get; set; }
        public int BookId{ get; set; }
        public bool CanBeDownloaded { get; set; }
    }
}