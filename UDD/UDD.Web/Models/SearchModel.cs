﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UDD.Web.Models
{
    public class SearchModel
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }
        public string Language { get; set; }
        public bool MatchAny { get; set; }
        public string Keywords { get; set; }
    }
}